# CppZadanie4
Zadanie 4 z Algorytmów i struktur danych

Korzystając z pliku dołączonego do zadania uzupełnij kod funkcji:

void _remove_all(node* root)  , której zadaniem jest usunięcie całej zawartości drzewa i zwolnienie pamięci.

node* search(node *root, int data) - której zadaniem jest znalezienie odpowiedniego węzła w drzewie binarnych poszukiwań podanego jako argument data.

Uzupełnij kod w funkcji main, tak aby udowadniał poprawność działania powyższych funkcji (Jeśli będzie to konieczne).

Uwaga jeśli ktoś korzysta bezpośrednio z kompilatora gcc kod możemy skompilować używając następującego wiersza polecenia:

gcc binary_search_tree_fill_the_blanks.c -Wall -Wextra -pedantic -o bst
